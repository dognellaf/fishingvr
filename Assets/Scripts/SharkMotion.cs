﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkMotion : MonoBehaviour
{
    public bool MovingZ;
    void Start()
    {
        MovingZ = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (MovingZ == true)
        {
         if (transform.position.z < 30)
            {
            transform.position = new Vector3(transform.position.x, transform.position.y, (float)(transform.position.z + 0.03));
            }
         if (transform.position.z >= 30)
            {
                transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y - 180, transform.rotation.z, transform.rotation.w);
                MovingZ = false;
            }
        }
        else
        {
         if (transform.position.z > -70)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, (float)(transform.position.z - 0.03));
            }
         if (transform.position.z <= -70)
            {
                transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y + 180, transform.rotation.z, transform.rotation.w);
                MovingZ = true;
            }
        }
    }
}
