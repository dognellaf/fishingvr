﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fishing : MonoBehaviour
{
    public GameObject Controller;
    private bool FishingTrue;
    private int _typeOfFish;
    public int _fishingCount;
    private bool _fishingBool;
    public int _fishingChance;
    public int _fishcount1;
    public int _fishcount2;
    public int _fishcount3;
    void Start()
    {
        FishingTrue = false;
        _fishcount1 = 0;
        _fishcount2 = 0;
        _fishcount3 = 0;
    }
    void Update()
    {
        FishingTrue = Controller.GetComponent<ControllerUI>().FishingTrue;
        if (FishingTrue == true)
        {
            if (_fishingBool != true)
            {
                if (_fishingCount == 0)
                    {
                    _fishingChance = Random.Range(0, 101000);
                    if (_fishingChance > 100700)
                    {
                        _fishingBool = true;
                        _fishingCount = 0;
                    }
                }
            }
            else
            {
                _fishingCount += 1;
                if (_fishingCount % 2 == 0)
                {
                    Controller.GetComponent<ControllerUI>().ColisionPoint = new Vector3(Controller.GetComponent<ControllerUI>().ColisionPoint.x, (float)(Controller.GetComponent<ControllerUI>().ColisionPoint.y - 0.05), Controller.GetComponent<ControllerUI>().ColisionPoint.z);
                }
                else
                {
                    Controller.GetComponent<ControllerUI>().ColisionPoint = new Vector3(Controller.GetComponent<ControllerUI>().ColisionPoint.x, (float)(Controller.GetComponent<ControllerUI>().ColisionPoint.y + 0.05), Controller.GetComponent<ControllerUI>().ColisionPoint.z);
                }
                //94.367
                if (_fishingCount >= 1010)
                {
                    _fishingBool = false;
                    _fishingCount = 0;
                }
                }
        }
        else
        {
            if (_fishingCount != 0)
            {
                _typeOfFish = Random.Range(0, 99999);
                if (_typeOfFish < 22222)
                {
                    _fishcount1 += 1;
                }
                else
                {
                    if ((_typeOfFish >= 33333) && (_typeOfFish < 55555))
                    {
                        _fishcount2 += 1;
                    }
                    else
                    {
                        if ((_typeOfFish >= 66666) && (_typeOfFish < 88888))
                        {
                            _fishcount3 += 1;
                        }
                    }
                }
                _fishingCount = 0;
                _fishingBool = false;
                _fishingChance = 0;
            }
        }
    }
}
