﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingLine : MonoBehaviour
{
    private bool FishingTrue;
    private Vector3 _startVector;
    public GameObject Controller;
    void Start()
    {
        FishingTrue = false;
        _startVector = new Vector3((float)4.271, (float)0.325, (float)-0.5);
    }
    void Update()
    {
        FishingTrue = Controller.GetComponent<ControllerUI>().FishingTrue;
        if (FishingTrue == true)
        {
            GetComponent<LineRenderer>().SetPosition(0, _startVector); 
            GetComponent<LineRenderer>().SetPosition(1, new Vector3(Controller.GetComponent<ControllerUI>().ColisionPoint.x, (float)(Controller.GetComponent<ControllerUI>().ColisionPoint.y+0.05), Controller.GetComponent<ControllerUI>().ColisionPoint.z));
        }
        else
        {
            GetComponent<LineRenderer>().SetPosition(0, _startVector);
            GetComponent<LineRenderer>().SetPosition(1, _startVector);
        }
    }
}
