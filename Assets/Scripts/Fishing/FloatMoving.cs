﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatMoving : MonoBehaviour
{
    private bool FishingTrue;
    private Vector3 _startVector;
    public GameObject Controller;
    void Start()
    {
        FishingTrue = false;
        _startVector = new Vector3(0, 0, 0);
    }
    void Update()
    {
        FishingTrue = Controller.GetComponent<ControllerUI>().FishingTrue;
        if (FishingTrue == true)
        {
            transform.position = Controller.GetComponent<ControllerUI>().ColisionPoint;
        }
        else
        {
            transform.position = _startVector;
        }
    }
}
