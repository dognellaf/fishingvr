﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FishCounter : MonoBehaviour
{
    public GameObject counter1, counter2, counter3,ScoreCounter,AllFish1,AllFish2,AllFish3;
    void Update()
    {
        TextMeshPro textmp1 = counter1.GetComponent<TextMeshPro>();
        TextMeshPro textmp2 = counter2.GetComponent<TextMeshPro>();
        TextMeshPro textmp3 = counter3.GetComponent<TextMeshPro>();
        TextMeshPro score = ScoreCounter.GetComponent<TextMeshPro>();
        textmp1.text = string.Format("{0:0.##}", GetComponent<Fishing>()._fishcount1);
        textmp2.text = string.Format("{0:0.##}", GetComponent<Fishing>()._fishcount2);
        textmp3.text = string.Format("{0:0.##}", GetComponent<Fishing>()._fishcount3);
        score.text = "Общий счет: "+string.Format("{0:0.##}", GetComponent<Fishing>()._fishcount1 * 100 + GetComponent<Fishing>()._fishcount2 * 65 + GetComponent<Fishing>()._fishcount3 * 30);
        if (GetComponent<Fishing>()._fishcount1 >= 3)
        {
            AllFish1.SetActive(true);
        }
        if (GetComponent<Fishing>()._fishcount2 >= 7)
        {
            AllFish2.SetActive(true);
        }
        if (GetComponent<Fishing>()._fishcount3 >= 14)
        {
            AllFish3.SetActive(true);
        }
    }
}
