﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioStart : MonoBehaviour
{
    // Start is called before the first frame update
    private AudioSource AudioS;
    void Start()
    {
        AudioS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!AudioS.isPlaying)
        {
            AudioS.Play();
        }
    }
}
